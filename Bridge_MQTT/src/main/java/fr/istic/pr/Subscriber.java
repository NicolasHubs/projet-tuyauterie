package fr.istic.pr;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class Subscriber implements MqttCallback {
    private static final Logger LOGGER = Logger.getLogger("Bridge_MQTT");
    private static String RABBIT_MQ_URI = "amqp://ipr:64GbL3k7uc33QCtc@localhost:8082/mri";
    private static String MQTT_BROKER = "tcp://127.0.0.1:1883";
    private final static String QUEUE_NAME = "ipr.usine1";
    private Connection connection;
    private Channel channel;
    private ObjectMapper mapper;
    private Map<String, CoreData> cores;

    public Subscriber() throws MqttException, IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        setUpAmqp();
        mapper = new ObjectMapper();
        cores = new HashMap<>();
        MqttConnectOptions conOpt = new MqttConnectOptions();
        conOpt.setCleanSession(true);
        String MQTT_USERNAME = "casxgqpl";
        conOpt.setUserName(MQTT_USERNAME);
        String MQTT_PASSWORD = "WCqNfUS1rskA";
        conOpt.setPassword(MQTT_PASSWORD.toCharArray());

        String MQTT_CLIENT_ID = "mqtt.bridge";
        MqttClient client = new MqttClient(MQTT_BROKER, MQTT_CLIENT_ID, new MemoryPersistence());
        client.setCallback(this);
        client.connect(conOpt);
        LOGGER.info("Bridge_MQTT - Successfully connected to MQTT_BROKER: (" + MQTT_BROKER + ")");

        int qos = 1;
        String topic = "/#";
        client.subscribe(topic, qos);
    }

    /**
     * @see MqttCallback#connectionLost(Throwable)
     */
    public void connectionLost(Throwable cause) {
        System.out.println("Connection lost because: " + cause + ".");
        System.exit(1);
    }

    /**
     * @see MqttCallback#deliveryComplete(IMqttDeliveryToken)
     */
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    /**
     * @see MqttCallback#messageArrived(String, MqttMessage)
     */
    public void messageArrived(String topic, MqttMessage message) {
        String[] data = topic.split("core");
        byte[] messageByte = message.getPayload();
        String messageValue = new String(messageByte);
        if (data.length <= 1) {
            return;
        }

        data = data[1].split("/");

        if (data.length <= 1) {
            return;
        }

        String core_name = "\"core" + data[0] + "\"";

        CoreData coreData = cores.get(core_name);

        if (null == coreData) {
            coreData = new CoreData();
            coreData.setLocation(core_name);
            cores.put(core_name, coreData);
        }

        if ("temperature".equals(data[1])) {
            // Si une valeur est déjà existante on envoie meme sans radiation, on évite la perte d'info
            if (coreData.getTemperature() != -1.0)
                publish(core_name);

            coreData.setTemperature(Double.parseDouble(messageValue));
        } else if ("radiation".equals(data[1])) {
            // Si une valeur est déjà existante on envoie meme sans temperature, on évite la perte d'info
            if (coreData.getRadiation() != -1.0)
                publish(core_name);

            coreData.setRadiation(Double.parseDouble(messageValue));
        } else {
            return;
        }

        coreData.setTimestamp(System.currentTimeMillis());
        if (coreData.getTemperature() != -1.0 && coreData.getRadiation() != -1.0)
            publish(core_name);
    }

    private void publish(String core_name) {
        CoreData coreData = cores.get(core_name);
        try {
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(coreData);
            coreData.clear();
            channel.basicPublish("", QUEUE_NAME, MessageProperties.MINIMAL_BASIC, jsonInString.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUpAmqp() throws
            IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(RABBIT_MQ_URI);
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();
        float sec = (currentTime - startTime) / 1000F;
        int timeoutInSeconds = 30;
        while (sec < timeoutInSeconds) {
            try {
                connection = factory.newConnection();
                break;
            } catch (ConnectException e1) {
                LOGGER.info("Bridge_MQTT - Trying to connect to rabbitmq... (Timeout:" + sec + "s/" + timeoutInSeconds + "s)");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            currentTime = System.currentTimeMillis();
            sec = (currentTime - startTime) / 1000F;
        }

        if (sec > timeoutInSeconds) {
            LOGGER.severe("Bridge_MQTT - Could not connect to rabbitmq (" + RABBIT_MQ_URI + ")");
            System.exit(1);
        }
        LOGGER.info("Bridge_MQTT - Successfully connected to RABBIT_MQ: (" + RABBIT_MQ_URI + ")");
        channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    }

    public static void main(String[] args) throws
            MqttException, IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        String host = System.getenv("MQTT_HOST");
        String port = System.getenv("MQTT_PORT");
        if (null == host) {
            host = "127.0.0.1";
            port = "1883";
        }
        MQTT_BROKER = "tcp://" + host + ":" + port;

        host = System.getenv("RABBITMQ_HOST");
        port = System.getenv("RABBITMQ_PORT");
        if (null == host) {
            host = "localhost";
            port = "8082";
        }
        RABBIT_MQ_URI = "amqp://ipr:64GbL3k7uc33QCtc@" + host + ":" + port + "/mri";
        new Subscriber();
    }
}
