package fr.istic.pr;

public class CoreData {
    private String location;
    private double temperature;
    private double radiation;
    private long timestamp;

    public CoreData() {
        location = "";
        temperature = -1.0;
        radiation = -1.0;
        timestamp = -1L;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getRadiation() {
        return radiation;
    }

    public void setRadiation(double radiation) {
        this.radiation = radiation;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void clear() {
        temperature = -1.0;
        radiation = -1.0;
        timestamp = -1L;
    }

    @Override
    public String toString() {
        return "Core{" +
                "location=" + location +
                ", temperature=" + temperature +
                ", radiation=" + radiation +
                ", timestamp=" + timestamp +
                '}';
    }
}
