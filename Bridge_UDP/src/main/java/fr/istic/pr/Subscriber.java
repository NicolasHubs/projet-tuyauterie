package fr.istic.pr;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.InvalidProtocolBufferException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class Subscriber {
    private static final Logger LOGGER = Logger.getLogger("Bridge_UDP");
    private final static String QUEUE_NAME = "ipr.usine2";
    private static String RABBIT_MQ_URI = "amqp://ipr:64GbL3k7uc33QCtc@localhost:8082/mri";
    private static MulticastSocket socket;
    private Connection connection;
    private Channel channel;
    private static int i = 1; // TODO : Corrige temporairement l'erreur de nommage du prof (A supprimer quand ce sera corrigé)

    public Subscriber() throws IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        setUpAmqp();
        try {
            int UDP_PORT = 9090;
            socket = new MulticastSocket(UDP_PORT);
            String UDP_IPADDR = "224.3.2.1";
            InetAddress groupIP = InetAddress.getByName(UDP_IPADDR);
            socket.joinGroup(groupIP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        handleMessages();
    }

    private void handleMessages() throws IOException {
        CoreData coreData;
        String jsonInString;
        ObjectMapper mapper = new ObjectMapper();
        //noinspection InfiniteLoopStatement
        while (true) {
            coreData = instantiateCoreDataFromBinary(recevoirMessage());
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(coreData);
            //System.out.println(jsonInString);
            channel.basicPublish("", QUEUE_NAME, MessageProperties.MINIMAL_BASIC, jsonInString.getBytes(StandardCharsets.UTF_8));
            i = (i % 5) + 1; // TODO : Corrige temporairement l'erreur de nommage du prof (A supprimer quand ce sera corrigé)
        }
    }

    private static byte[] recevoirMessage() {
        byte[] messageContent = new byte[1024];
        DatagramPacket packet = new DatagramPacket(messageContent, messageContent.length);
        try {
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.copyOfRange(messageContent, 0, packet.getLength());
    }

    private void setUpAmqp() throws
            IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(RABBIT_MQ_URI);
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();
        float sec = (currentTime - startTime) / 1000F;
        int timeoutInSeconds = 30;
        while (sec < timeoutInSeconds) {
            try {
                connection = factory.newConnection();
                break;
            } catch (ConnectException e1) {
                LOGGER.info("Bridge_UDP - Trying to connect to rabbitmq... (Timeout:" + sec + "s/" + timeoutInSeconds + "s)");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            currentTime = System.currentTimeMillis();
            sec = (currentTime - startTime) / 1000F;
        }

        if (sec > timeoutInSeconds) {
            LOGGER.severe("Bridge_UDP - Could not connect to rabbitmq (" + RABBIT_MQ_URI + ")");
            System.exit(1);
        }
        LOGGER.info("Bridge_UDP - successfully connected to RABBIT_MQ: (" + RABBIT_MQ_URI + ")");
        channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    }

    private static CoreData instantiateCoreDataFromBinary(byte[] binaryCoreData) {
        CoreData coreDate = null;
        try {
            CoreDataProto.CoreData copiedCoreData = CoreDataProto.CoreData.parseFrom(binaryCoreData);
            coreDate = new CoreData.Builder()
                    .setLocation("\"core" + i/*copiedCoreData.getLocation()*/ + "\"") // TODO : Corrige temporairement l'erreur de nommage du prof (A supprimer quand ce sera corrigé)
                    .setRadiation(copiedCoreData.getRadiation())
                    .setTimestamp(copiedCoreData.getTimestamp())
                    .setTemperature(copiedCoreData.getTemperature())
                    .build();
        } catch (InvalidProtocolBufferException e) {
            LOGGER.severe("ERROR: Unable to instantiate CoreDataProto.CoreData instance from provided binary data - \n" + e);
        }
        return coreDate;
    }

    public static void main(String[] args) throws IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        String host = System.getenv("RABBITMQ_HOST");
        String port = System.getenv("RABBITMQ_PORT");
        if (null == host) {
            host = "localhost";
            port = "8082";
        }
        RABBIT_MQ_URI = "amqp://ipr:64GbL3k7uc33QCtc@" + host + ":" + port + "/mri";
        new Subscriber();
    }
}
