package fr.istic.pr;

public class CoreData {
    private String location;
    private double temperature;
    private double radiation;
    private long timestamp;

    private CoreData(String location, double temperature, double radiation, long timestamp) {
        this.location = location;
        this.temperature = temperature;
        this.radiation = radiation;
        this.timestamp = timestamp;
    }

    public String getLocation() {
        return location;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getRadiation() {
        return radiation;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Core{" +
                "location =" + location +
                ", temperature=" + temperature +
                ", radiation=" + radiation +
                ", timestamp=" + timestamp +
                '}';
    }

    public static class Builder {
        private String location;
        private double temperature;
        private double radiation;
        private long timestamp;

        public Builder() {
            location = "";
            temperature = 0.0;
            radiation = 0.0;
            timestamp = 0L;
        }

        public Builder setLocation(String location) {
            this.location = location;
            return this;
        }

        public Builder setTemperature(double temperature) {
            this.temperature = temperature;
            return this;
        }

        public Builder setRadiation(double radiation) {
            this.radiation = radiation;
            return this;
        }

        public Builder setTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public CoreData build() {
            return new CoreData(location, temperature, radiation, timestamp);
        }

    }
}
