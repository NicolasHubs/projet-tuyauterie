# Pour lancer l'usine :

```bash
./run.sh
```

# Pour télécharger protoc :

```bash
./getprotoc.sh
```

Vous trouverez le binaire dans bin