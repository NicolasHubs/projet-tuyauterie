#!/bin/bash
docker-compose down --remove-orphans
sudo docker rm -vf influxdb
mkdir -p data/grafana
sudo docker rm -vf grafana
sudo docker run --rm -it -v $(pwd)/data:/data alpine /bin/sh -c "(cd data && rm -fr /data/*)"
docker-compose up --build
