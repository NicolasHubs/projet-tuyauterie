RADIATION = 2;
TEMPERATURE = 3;
TIMESTAMP = 4;

usines = ["ipr_usine1", "ipr_usine2"];
cores = ["core1", "core2", "core3", "core4", "core5"];



//TODO : DELETE LE CHILD AVANT DE LES REMPLIR À NOUVEAU
//TODO : GÉRER LA BDD RECUPERER LES VALEURS DE CHAQUE COEUR
$(document).ready(function() {
    getValues();
    window.setInterval(function() {
        getValues()
    }, 10000);
});

function getValues(){

    usines.forEach(function(usine){
        cores.forEach(function(core){
            $.ajax({
                url: "/values?usine="+usine+"&core="+core
            }).done(function (data) {

                value = data.results[0].series[0].values;
                rad = getValue(value, RADIATION).reverse();
                temp = getValue(value, TEMPERATURE).reverse();
                time = getValue(value, TIMESTAMP).reverse();

                console.log(temp);
                console.log(rad);
                console.log(time);
                Highcharts.chart(usine+'_'+core, {
                    title: {
                        text: value[0][1]
                    },
                    xAxis: [{
                        categories: time,
                        crosshair: true
                    }],
                    yAxis: [{ // Primary yAxis
                        labels: {
                            format: '{value}°C',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Temperature',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                    }, { // Secondary yAxis
                        title: {
                            text: 'Radiation',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} Sv',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true,
                        type: 'logarithmic'
                    }],
                    tooltip: {
                        shared: true
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'left',
                        x: 120,
                        verticalAlign: 'top',
                        y: 100,
                        floating: true,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
                    },
                    series: [{
                        name: 'Radiation',
                        type: 'column',
                        animation: false,
                        yAxis: 1,
                        data: rad,
                        tooltip: {
                            valueSuffix: ' Sv'
                        }

                    }, {
                        name: 'Temperature',
                        type: 'spline',
                        animation: false,
                        data: temp,
                        tooltip: {
                            valueSuffix: '°C'
                        }
                    }]
                });
            });
        });
    });
}

function getValue(data, type){
    var tab = [];
    data.forEach(function(d){
        if(type === TIMESTAMP){
            var date = new Date(d[type]);
            var hours = date.getHours();
            var minutes = "0" + date.getMinutes();
            var seconds = "0" + date.getSeconds();
            tab.push(hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2));
        }
        else {
            tab.push(d[type]);
        }
    });
    return tab;
}
