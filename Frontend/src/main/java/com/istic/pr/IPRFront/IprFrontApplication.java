package com.istic.pr.IPRFront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IprFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(IprFrontApplication.class, args);
	}

}
