package com.istic.pr.IPRFront.controllers;


import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@EnableAutoConfiguration
@RestController
public class ApiController {
    private InfluxDB influxDB;

    ApiController(){
        String influxdb_ipaddr = System.getenv("INFLUXDB_HOST");
        String influxdb_port = System.getenv("INFLUXDB_PORT");
        if (null == influxdb_ipaddr) {
            influxdb_ipaddr = "localhost";
            influxdb_port = "8086";
        }
        this.influxDB = InfluxDBFactory.connect("http://"+influxdb_ipaddr+":"+influxdb_port);
    }

    @GetMapping(value = "/values", produces = MediaType.APPLICATION_JSON_VALUE)
    public QueryResult getUsine1( @RequestParam String usine, @RequestParam String core){
        Query query = new Query("SELECT * FROM \"core\" WHERE (\"location\" = '"+core+"') ORDER BY time DESC LIMIT 10", usine);
        Logger.getGlobal().info(query.getCommand()+" on : "+query.getDatabase());
        QueryResult queryResult = this.influxDB.query(query);
        return queryResult;
    }
}
