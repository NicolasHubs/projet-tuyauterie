#!/bin/bash

## Ce script va changer le fichier password.txt !
## ne lancer que si vous savez ce que vous faites.
## https://medium.com/@eranda/setting-up-authentication-on-mosquitto-mqtt-broker-de5df2e29afc
## http://www.steves-internet-guide.com/mqtt-username-password-example/

sudo docker run --name encrypt --rm -it -v $(pwd)/password.txt:/password.txt --entrypoint="/bin/sh" eclipse-mosquitto -c "mosquitto_passwd -b /password.txt casxgqpl WCqNfUS1rskA"