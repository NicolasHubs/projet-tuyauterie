#!/bin/bash
mvn package 
mvn dependency:copy-dependencies
sudo docker rm -vf mosquitto
sudo docker run --name mosquitto -d -p 8084:1883 -v $(pwd)/password.txt:/mosquitto/config/password.txt -v $(pwd)/mosquitto.conf:/mosquitto/config/mosquitto.conf -p 8085:9001 eclipse-mosquitto
java -classpath target/*:target/dependency/* mqtt.sensor.App
