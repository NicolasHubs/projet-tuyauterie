package fr.istic.pr;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class Consumer {
    private static final Logger LOGGER = Logger.getLogger("Collecteur");
    private final static String QUEUE_NAME_USINE1 = "ipr.usine1";
    private final static String QUEUE_NAME_USINE2 = "ipr.usine2";
    private final static String DATABASE_USINE1 = "ipr_usine1";
    private final static String DATABASE_USINE2 = "ipr_usine2";
    private static String influxdb_ipaddr = "localhost";
    private static String influxdb_port = "8086";
    private static String RABBIT_MQ_URI = "amqp://ipr:64GbL3k7uc33QCtc@rabbitmq:8082/mri";
    private Connection connection;

    public Consumer() throws IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        setUpAmqp();
    }

    private static void createDatabase(String database_Name) {
        Client client = ClientBuilder.newClient();
        Response response = client.target("http://" + influxdb_ipaddr + ":" + influxdb_port)
                .path("query")
                .queryParam("q", "CREATE DATABASE " + database_Name)
                .request()
                .post(Entity.text(""));
        if (response.getStatus() == 200)
            LOGGER.info("Database " + database_Name + " successfully created.");
    }

    private static void postValue(String database_Name, String data) {
        //String result = "curl -i -XPOST \"http://localhost:8086/write?db=" + database_Name + "\" --data-binary '" + data + "'";
        System.out.println(data);
        Client client = ClientBuilder.newClient();
        client.target("http://" + influxdb_ipaddr + ":" + influxdb_port)
                .path("write")
                .queryParam("db", database_Name)
                .request()
                .post(Entity.text(data));
        /*System.out.println("target before posting : " + target.getUri());

        System.out.println(target.request()
                .post(Entity.text(data)).getStatus() + " target after posting : " + target.getUri());*/
    }

    //curl -G 'http://localhost:8086/query?db=ipr_usine1' --data-urlencode 'q=SELECT * FROM "core"'
    @SuppressWarnings("unused")
    public static void getAllValues(String database_Name) {
        Client client = ClientBuilder.newClient();
        Response response = client.target("http://" + influxdb_ipaddr + ":" + influxdb_port)
                .path("query")
                .queryParam("db", database_Name)
                .queryParam("q", "SELECT * FROM core")
                .request()
                .get();
        System.out.println(response.readEntity(String.class));
    }

    private static String parseMap(Map myMap) {
        String result = myMap.toString()
                .replace("{", "")
                .replace("}", "")
                .replace(", ", ",");
        return String.format(Locale.ROOT, "core %s", result);
    }

    private void setUpAmqp() throws
            IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(RABBIT_MQ_URI);
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();
        float sec = (currentTime - startTime) / 1000F;
        int timeoutInSeconds = 30;
        while (sec < timeoutInSeconds) {
            try {
                connection = factory.newConnection();
                break;
            } catch (ConnectException e1) {
                LOGGER.info("Collecteur - Trying to connect to rabbitmq... (Timeout:" + sec + "s/" + timeoutInSeconds + "s)");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            currentTime = System.currentTimeMillis();
            sec = (currentTime - startTime) / 1000F;
        }

        if (sec > timeoutInSeconds) {
            LOGGER.severe("Collecteur - Could not connect to rabbitmq (" + RABBIT_MQ_URI + ")");
            System.exit(1);
        }
        LOGGER.info("Collecteur - successfully connected to RABBIT_MQ: (" + RABBIT_MQ_URI + ")");

        Channel channel_usine1 = connection.createChannel();
        channel_usine1.queueDeclare(QUEUE_NAME_USINE1, false, false, false, null);
        DeliverCallback deliverCallback_usine1 = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            byte[] mapData = message.getBytes();
            Map myMap;
            ObjectMapper objectMapper = new ObjectMapper();
            myMap = objectMapper.readValue(mapData, Map.class);
            postValue(DATABASE_USINE1, parseMap(myMap));
        };
        channel_usine1.basicConsume(QUEUE_NAME_USINE1, true, deliverCallback_usine1, consumerTag -> {
        });

        Channel channel_usine2 = connection.createChannel();
        channel_usine2.queueDeclare(QUEUE_NAME_USINE2, false, false, false, null);
        DeliverCallback deliverCallback_usine2 = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            byte[] mapData = message.getBytes();
            Map myMap;
            ObjectMapper objectMapper = new ObjectMapper();
            myMap = objectMapper.readValue(mapData, Map.class);
            postValue(DATABASE_USINE2, parseMap(myMap));
        };

        channel_usine2.basicConsume(QUEUE_NAME_USINE2, true, deliverCallback_usine2, consumerTag -> {
        });
    }

    public static void main(String[] argv) throws URISyntaxException, KeyManagementException, TimeoutException, NoSuchAlgorithmException, IOException {
        String host = System.getenv("RABBITMQ_HOST");
        String port = System.getenv("RABBITMQ_PORT");
        if (null == host) {
            host = "localhost";
            port = "5672";
        }
        RABBIT_MQ_URI = "amqp://ipr:64GbL3k7uc33QCtc@" + host + ":" + port + "/mri";

        influxdb_ipaddr = System.getenv("INFLUXDB_HOST");
        influxdb_port = System.getenv("INFLUXDB_PORT");
        if (null == influxdb_ipaddr) {
            influxdb_ipaddr = "localhost";
            influxdb_port = "8086";
        }
        createDatabase(DATABASE_USINE1);
        createDatabase(DATABASE_USINE2);

        new Consumer();
    }
}

